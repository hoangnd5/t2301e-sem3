﻿namespace WebAppGitlab.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Image { get; set; }

        public string Description { get; set; }
    }
}
